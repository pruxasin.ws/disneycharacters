# DisneyCharacters


## Requirements
•  Android 8.1 (API 27) and above\
•  Internet Connection

## How to build and run the application
1. Clone the repository:
```
git clone https://gitlab.com/pruxasin.ws/disneycharacters.git
```
2. Change the run configuration to 'app' and click the Run button
