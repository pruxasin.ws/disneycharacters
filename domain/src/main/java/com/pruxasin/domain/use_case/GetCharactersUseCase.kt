package com.pruxasin.domain.use_case

import androidx.paging.PagingData
import com.pruxasin.domain.model.characters.CharacterModel
import kotlinx.coroutines.flow.Flow

interface GetCharactersUseCase {
    suspend fun getCharacters(): Flow<PagingData<CharacterModel>>
    suspend fun insertFavoriteCharacter(characterModel: CharacterModel)
    suspend fun deleteFavoriteCharacter(characterId: Int)
    fun getFavoriteCharacters(): Flow<List<CharacterModel>>
}