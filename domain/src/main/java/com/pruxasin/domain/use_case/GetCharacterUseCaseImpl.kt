package com.pruxasin.domain.use_case

import com.pruxasin.domain.model.characters.CharacterModel
import com.pruxasin.domain.repository.CharactersRepository
import com.pruxasin.domain.util.Resource
import kotlinx.coroutines.flow.Flow

class GetCharacterUseCaseImpl(
    private val charactersRepository: CharactersRepository
): GetCharacterUseCase {
    override suspend fun getCharacter(characterId: Int): Flow<Resource<CharacterModel>> {
        return charactersRepository.getCharacter(characterId)
    }
}