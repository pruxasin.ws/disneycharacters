package com.pruxasin.domain.use_case

import androidx.paging.PagingData
import com.pruxasin.domain.model.characters.CharacterModel
import com.pruxasin.domain.repository.CharactersRepository
import kotlinx.coroutines.flow.Flow

class GetCharactersUseCaseImpl(
    private val charactersRepository: CharactersRepository
): GetCharactersUseCase  {
    override suspend fun getCharacters(): Flow<PagingData<CharacterModel>> {
        return charactersRepository.getCharacters()
    }

    override suspend fun insertFavoriteCharacter(characterModel: CharacterModel) {
        charactersRepository.insertFavoriteCharacter(characterModel)
    }

    override suspend fun deleteFavoriteCharacter(characterId: Int) {
        charactersRepository.deleteFavoriteCharacter(characterId)
    }

    override fun getFavoriteCharacters(): Flow<List<CharacterModel>> {
        return charactersRepository.getFavoriteCharacters()
    }
}