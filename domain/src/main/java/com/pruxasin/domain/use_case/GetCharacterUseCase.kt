package com.pruxasin.domain.use_case

import com.pruxasin.domain.model.characters.CharacterModel
import com.pruxasin.domain.util.Resource
import kotlinx.coroutines.flow.Flow

interface GetCharacterUseCase {
    suspend fun getCharacter(characterId: Int): Flow<Resource<CharacterModel>>
}