package com.pruxasin.domain.model.characters

data class CharactersModel(
    val data: List<CharacterModel>,
)
