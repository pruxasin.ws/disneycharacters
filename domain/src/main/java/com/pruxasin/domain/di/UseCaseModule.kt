package com.pruxasin.domain.di

import com.pruxasin.domain.use_case.GetCharacterUseCase
import com.pruxasin.domain.use_case.GetCharacterUseCaseImpl
import com.pruxasin.domain.use_case.GetCharactersUseCase
import com.pruxasin.domain.use_case.GetCharactersUseCaseImpl
import org.koin.dsl.module

val useCaseModule = module {
    factory<GetCharactersUseCase> { GetCharactersUseCaseImpl(get()) }
    factory<GetCharacterUseCase> { GetCharacterUseCaseImpl(get()) }
}