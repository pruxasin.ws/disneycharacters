package com.pruxasin.domain.repository

import androidx.paging.PagingData
import com.pruxasin.domain.model.characters.CharacterModel
import com.pruxasin.domain.util.Resource
import kotlinx.coroutines.flow.Flow

interface CharactersRepository {
    suspend fun getCharacters(): Flow<PagingData<CharacterModel>>
    suspend fun insertFavoriteCharacter(character: CharacterModel)
    suspend fun deleteFavoriteCharacter(characterId: Int)
    fun getFavoriteCharacters(): Flow<List<CharacterModel>>
    suspend fun getCharacter(characterId: Int): Flow<Resource<CharacterModel>>
}