package com.pruxasin.data.remote.info

data class InfoDto(
    val count: Int,
    val totalPages: Int,
    val previousPage: String? = null,
    val nextPage: String? = null
)