package com.pruxasin.data.remote.dto.characters

import com.pruxasin.domain.model.characters.CharacterModel

data class CharacterDto(
    val data: CharacterData
)

data class CharacterData(
    val _id: Int,
    val allies: List<String>,
    val enemies: List<String>,
    val films: List<String>,
    val imageUrl: String? = "",
    val name: String,
    val parkAttractions: List<String>,
    val shortFilms: List<String>,
    val tvShows: List<String>,
    val url: String,
    val videoGames: List<String>
) {
    fun toCharacterModel(): CharacterModel {
        return CharacterModel(
            _id = _id,
            allies = allies,
            enemies = enemies,
            films = films,
            imageUrl = imageUrl,
            name = name,
            parkAttractions = parkAttractions,
            shortFilms = shortFilms,
            tvShows = tvShows,
            videoGames = videoGames
        )
    }
}