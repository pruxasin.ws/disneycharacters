package com.pruxasin.data.remote.service

import com.pruxasin.data.remote.dto.characters.CharacterDto
import com.pruxasin.data.remote.dto.characters.CharactersDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    companion object {
        const val BASE_URL = "https://api.disneyapi.dev"
    }

    @GET("character")
    suspend fun getCharacters(
        @Query("page") page: Int
    ): Response<CharactersDto>

    @GET("character/{characterId}")
    suspend fun getCharacter(
        @Path("characterId") characterId: Int
    ): CharacterDto
}