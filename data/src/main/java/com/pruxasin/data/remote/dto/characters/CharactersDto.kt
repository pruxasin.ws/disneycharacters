package com.pruxasin.data.remote.dto.characters

import com.pruxasin.data.remote.info.InfoDto
import com.pruxasin.domain.model.characters.CharactersModel

data class CharactersDto(
    val data: List<CharacterData>,
    val info: InfoDto
) {
    fun toCharactersModel(): CharactersModel {
        return CharactersModel(
            data = data.map { it.toCharacterModel() }
        )
    }
}