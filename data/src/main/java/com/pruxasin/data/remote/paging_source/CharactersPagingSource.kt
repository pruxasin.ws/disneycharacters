package com.pruxasin.data.remote.paging_source

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.pruxasin.data.remote.service.ApiService
import com.pruxasin.domain.model.characters.CharacterModel

class CharactersPagingSource(
    private val apiService: ApiService,
) : PagingSource<Int, CharacterModel>() {
    override fun getRefreshKey(state: PagingState<Int, CharacterModel>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CharacterModel> {
        return try {
            val pageNumber = params.key ?: 1
            val response = apiService.getCharacters(page = pageNumber)
            val data = response.body()?.data?.map { it.toCharacterModel() }?.toMutableList()
            if (pageNumber == 1) {
                data?.add(0, CharacterModel(
                    header = "All Characters",
                    _id = 0,
                    allies = emptyList(),
                    enemies = emptyList(),
                    films = emptyList(),
                    imageUrl = "",
                    name = "",
                    parkAttractions = emptyList(),
                    shortFilms = emptyList(),
                    tvShows = emptyList(),
                    videoGames = emptyList()
                ))
            }
            LoadResult.Page(
                data = (data ?: emptyList()).toList(),
                prevKey = if (pageNumber == 1) null else pageNumber - 1,
                nextKey = if (pageNumber < (response.body()?.info?.totalPages ?: 1)) pageNumber + 1 else null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}