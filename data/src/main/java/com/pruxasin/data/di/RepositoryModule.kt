package com.pruxasin.data.di

import com.pruxasin.data.repository_impl.CharactersRepositoryImpl
import com.pruxasin.domain.repository.CharactersRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<CharactersRepository> { CharactersRepositoryImpl(get(), get()) }
}