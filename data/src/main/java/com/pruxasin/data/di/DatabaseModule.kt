package com.pruxasin.data.di

import androidx.room.Room
import com.pruxasin.data.local.database.FavoriteDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single { Room.databaseBuilder(androidContext(), FavoriteDatabase::class.java, "favorite_db").build() }
    factory { get<FavoriteDatabase>().dao() }
}