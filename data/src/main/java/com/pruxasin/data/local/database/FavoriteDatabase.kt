package com.pruxasin.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pruxasin.data.local.dao.FavoriteDao
import com.pruxasin.data.local.entity.CharacterEntity
import com.pruxasin.data.local.util.Converter

@Database(entities = [CharacterEntity::class], version = 1, exportSchema = false)
@TypeConverters(Converter::class)
abstract class FavoriteDatabase: RoomDatabase() {
    abstract fun dao(): FavoriteDao
}