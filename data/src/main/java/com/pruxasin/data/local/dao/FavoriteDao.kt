package com.pruxasin.data.local.dao

import androidx.room.*
import com.pruxasin.data.local.entity.CharacterEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface FavoriteDao {
    @Query("SELECT * FROM favorite")
    fun getFavoriteCharacters(): Flow<List<CharacterEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertFavoriteCharacter(characterEntity: CharacterEntity)

    @Query("DELETE FROM favorite WHERE ID=:characterId")
    suspend fun deleteFavoriteCharacter(characterId: Int)
}