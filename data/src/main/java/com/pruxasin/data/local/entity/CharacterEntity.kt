package com.pruxasin.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pruxasin.domain.model.characters.CharacterModel

@Entity(tableName = "favorite")
data class CharacterEntity(
    @PrimaryKey
    var id: Int,
    val allies: List<String>,
    val enemies: List<String>,
    val films: List<String>,
    val imageUrl: String? = "",
    val name: String,
    val parkAttractions: List<String>,
    val shortFilms: List<String>,
    val tvShows: List<String>,
    val videoGames: List<String>
) {
    fun toCharacterModel(): CharacterModel {
        return CharacterModel(
            _id = id,
            allies = allies,
            enemies = enemies,
            films = films,
            imageUrl = imageUrl,
            name = name,
            parkAttractions = parkAttractions,
            shortFilms = shortFilms,
            tvShows = tvShows,
            videoGames = videoGames
        )
    }
}
