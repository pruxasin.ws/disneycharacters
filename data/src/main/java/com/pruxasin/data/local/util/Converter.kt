package com.pruxasin.data.local.util

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converter {
    private val gson = Gson()

    @TypeConverter
    fun fromStringListJson(json: String): List<String> {
        return gson.fromJson(json, object : TypeToken<ArrayList<String>>(){}.type)
    }

    @TypeConverter
    fun toStringListJson(somethings: List<String>): String {
        return gson.toJson(somethings, object : TypeToken<ArrayList<String>>(){}.type)
    }
}