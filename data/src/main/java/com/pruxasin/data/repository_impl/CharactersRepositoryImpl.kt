package com.pruxasin.data.repository_impl

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.pruxasin.data.local.dao.FavoriteDao
import com.pruxasin.data.local.entity.CharacterEntity
import com.pruxasin.data.remote.paging_source.CharactersPagingSource
import com.pruxasin.data.remote.service.ApiService
import com.pruxasin.domain.model.characters.CharacterModel
import com.pruxasin.domain.repository.CharactersRepository
import com.pruxasin.domain.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import retrofit2.HttpException
import java.io.IOException

class CharactersRepositoryImpl(
    private val apiService: ApiService,
    private val favoriteDao: FavoriteDao
) : CharactersRepository {
    override suspend fun getCharacters(): Flow<PagingData<CharacterModel>> {
        return Pager(
            config = PagingConfig(pageSize = 1),
            initialKey = 1
        ) {
            CharactersPagingSource(apiService)
        }.flow
    }

    override suspend fun insertFavoriteCharacter(character: CharacterModel) {
        favoriteDao.insertFavoriteCharacter(
            CharacterEntity(
                id = character._id,
                allies = character.allies,
                enemies = character.enemies,
                films = character.films,
                imageUrl = character.imageUrl,
                name = character.name,
                parkAttractions = character.parkAttractions,
                shortFilms = character.shortFilms,
                tvShows = character.tvShows,
                videoGames = character.videoGames
            )
        )
    }

    override suspend fun deleteFavoriteCharacter(characterId: Int) {
        favoriteDao.deleteFavoriteCharacter(characterId)
    }

    override fun getFavoriteCharacters(): Flow<List<CharacterModel>> {
        return favoriteDao.getFavoriteCharacters().map {
            it.map { it.toCharacterModel() }
        }
    }

    override suspend fun getCharacter(characterId: Int): Flow<Resource<CharacterModel>> = flow {
        emit(Resource.Loading())

        try {
            val character = apiService.getCharacter(characterId).data.toCharacterModel()
            emit(Resource.Success(character))
        } catch (httpException: HttpException) {
            emit(Resource.Error(message = httpException.message.toString()))
        } catch (ioException: IOException) {
            emit(Resource.Error(message = ioException.message.toString()))
        }
    }
}