package com.pruxasin.disneycharacters.util

sealed class ViewState<out T: Any> {
    class Loading<out T: Any>: ViewState<T>()
    data class Success<out T: Any>(val data: T): ViewState<T>()
    data class Error<out T: Any>(val message: String): ViewState<T>()
}