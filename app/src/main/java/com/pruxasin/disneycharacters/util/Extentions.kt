package com.pruxasin.disneycharacters.util

import android.util.Log
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.pruxasin.domain.model.characters.CharacterModel

fun CharacterModel.getCharacterDescription(): String {
    var description = ""
    this.films.let {
        description += "Films:\n"
        description += appendDescription(stringList = it)
    }
    this.shortFilms.let {
        description += "\nShort Films:\n"
        description += appendDescription(stringList = it)
    }
    this.tvShows.let {
        description += "\nTV Shows:\n"
        description += appendDescription(stringList = it)
    }
    this.videoGames.let {
        description += "\nVideo Games:\n"
        description += appendDescription(stringList = it)
    }
    this.parkAttractions.let {
        description += "\nPark Attractions:\n"
        description += appendDescription(stringList = it)
    }
    this.allies.let {
        description += "\nAllies:\n"
        description += appendDescription(stringList = it)
    }
    this.enemies.let {
        description += "\nEnemies:\n"
        description += appendDescription(stringList = it)
    }
    return description
}

private fun appendDescription(stringList: List<String>): String {
    var appendedString = ""
    if (stringList.isEmpty()) {
        appendedString += " -\n"
        return appendedString
    }
    stringList.forEach { st ->
        appendedString += " • $st\n"
    }
    return appendedString
}

fun RecyclerView.attachFab(fab : FloatingActionButton) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if ((recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition() < 5) {
                fab.hide()
            } else {
                if (dy > 0 ){
                    if (fab.isShown) fab.hide()
                } else {
                    if (!fab.isShown) fab.show()
                }
            }
        }
    })
}

fun CombinedLoadStates.decideOnState(
//    onLoading: (Boolean) -> Unit,
    onSuccess: (Boolean) -> Unit,
    onError: (Boolean) -> Unit,
    itemCount: Int
) {
//    onLoading(refresh is LoadState.Loading)

    val isError = refresh is LoadState.Error
    onError(isError)

    onSuccess(source.prepend is LoadState.NotLoading && itemCount == 0 && !isError)
}