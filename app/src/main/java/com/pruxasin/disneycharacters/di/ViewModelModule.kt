package com.pruxasin.disneycharacters.di

import com.pruxasin.disneycharacters.view_model.CharacterViewModel
import com.pruxasin.disneycharacters.view_model.CharactersViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { CharactersViewModel(get()) }
    viewModel { CharacterViewModel(get()) }
}