package com.pruxasin.disneycharacters.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pruxasin.disneycharacters.util.ViewState
import com.pruxasin.domain.model.characters.CharacterModel
import com.pruxasin.domain.use_case.GetCharacterUseCase
import com.pruxasin.domain.util.Resource
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class CharacterViewModel(
    private val getCharacterUseCase: GetCharacterUseCase
): ViewModel() {

    private val _state = MutableSharedFlow<ViewState<CharacterModel>>()
    val state = _state.asSharedFlow()

    fun getCharacter(characterId: Int) {
        viewModelScope.launch {
            getCharacterUseCase.getCharacter(characterId).onEach { result ->
                when (result) {
                    is Resource.Loading -> {
                        _state.emit(ViewState.Loading())
                    }
                    is Resource.Success -> {
                        _state.emit(ViewState.Success(data = result.data))
                    }

                    is Resource.Error -> {
                        _state.emit(ViewState.Error(message = result.message))
                    }
                }
            }.launchIn(this)
        }
    }
}