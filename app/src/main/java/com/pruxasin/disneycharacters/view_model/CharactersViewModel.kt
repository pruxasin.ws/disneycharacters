package com.pruxasin.disneycharacters.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.pruxasin.domain.model.characters.CharacterModel
import com.pruxasin.domain.use_case.GetCharactersUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class CharactersViewModel(
    private val getCharactersUseCase: GetCharactersUseCase
): ViewModel() {

    suspend fun getCharacters(): Flow<PagingData<CharacterModel>> = getCharactersUseCase.getCharacters().cachedIn(viewModelScope)

    fun getFavoriteCharacters() = getCharactersUseCase.getFavoriteCharacters()

    fun insertFavoriteCharacter(characterModel: CharacterModel) = viewModelScope.launch {
        getCharactersUseCase.insertFavoriteCharacter(characterModel)
    }

    fun deleteFavoriteCharacter(characterId: Int) = viewModelScope.launch {
        getCharactersUseCase.deleteFavoriteCharacter(characterId)
    }
}