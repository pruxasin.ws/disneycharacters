package com.pruxasin.disneycharacters

import android.app.Application
import com.pruxasin.data.di.databaseModule
import com.pruxasin.data.di.networkingModule
import com.pruxasin.data.di.repositoryModule
import com.pruxasin.disneycharacters.di.viewModelModule
import com.pruxasin.domain.di.useCaseModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DisneyCharactersApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@DisneyCharactersApplication)
            modules(appModules + domainModules + dataModules)
        }
    }
}

val appModules = listOf(viewModelModule)
val dataModules = listOf(networkingModule, repositoryModule, databaseModule)
val domainModules = listOf(useCaseModule)
