package com.pruxasin.disneycharacters.view.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.pruxasin.disneycharacters.R
import com.pruxasin.disneycharacters.databinding.FragmentCharactersBinding
import com.pruxasin.disneycharacters.util.attachFab
import com.pruxasin.disneycharacters.util.decideOnState
import com.pruxasin.disneycharacters.view.main.adapter.CharactersPagingAdapter
import com.pruxasin.disneycharacters.view.main.adapter.FavoriteCharactersAdapter
import com.pruxasin.disneycharacters.view.main.adapter.FooterStateAdapter
import com.pruxasin.disneycharacters.view.main.adapter.TryAgainAdapter
import com.pruxasin.disneycharacters.view_model.CharactersViewModel
import com.pruxasin.domain.model.characters.CharacterModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class CharactersFragment: Fragment() {
    private lateinit var binding: FragmentCharactersBinding

    private val charactersViewModel: CharactersViewModel by viewModel()
    private val charactersPagingAdapter = CharactersPagingAdapter()
    private val footerStateAdapter = FooterStateAdapter()
    private val favoriteCharactersAdapter = FavoriteCharactersAdapter()
    private val tryAgainAdapter = TryAgainAdapter()
    private val spanCount = 2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!this::binding.isInitialized) {
            binding = FragmentCharactersBinding.inflate(layoutInflater, container, false)
            setupView()
            observeData()
        }
        return binding.root
    }

    private fun observeData() {
        lifecycleScope.launch {
            charactersViewModel.getCharacters().collectLatest {
                launch(Dispatchers.Main) {
                    charactersPagingAdapter.loadStateFlow.collectLatest { loadStates ->
                        if (loadStates.refresh is LoadState.Loading) {
                            binding.characterRecyclerView.isVisible = false
                            binding.loadingProgressBar.isVisible = true
                        } else if (loadStates.source.append is LoadState.NotLoading){
                            if (binding.loadingProgressBar.isVisible && !binding.characterRecyclerView.isVisible) {
                                binding.loadingProgressBar.isVisible = false
                                binding.characterRecyclerView.isVisible = true
                            }
                        }
                    }
                }
                charactersPagingAdapter.submitData(lifecycle, it)
            }
        }

        charactersViewModel.getFavoriteCharacters()
            .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach {
                val charactersMutableList = it.toMutableList()
                if (charactersMutableList.isNotEmpty()) {
                    charactersMutableList.add(0, CharacterModel(
                        header = "Favorites Characters",
                        _id = 0,
                        allies = emptyList(),
                        enemies = emptyList(),
                        films = emptyList(),
                        imageUrl = "",
                        name = "",
                        parkAttractions = emptyList(),
                        shortFilms = emptyList(),
                        tvShows = emptyList(),
                        videoGames = emptyList()
                    ))
                }
                favoriteCharactersAdapter.setData(characters = charactersMutableList)
            }.launchIn(lifecycleScope)
    }

    private fun setupView() {
        binding.actionBarView.arrowBackImageView.visibility = View.INVISIBLE
        binding.actionBarView.titleTextView.text = getString(R.string.disney_characters)

        binding.fabButton.setOnClickListener {
            binding.characterRecyclerView.smoothScrollToPosition(0)
        }

        setupCharactersRecyclerView()
    }

    private fun setupCharactersRecyclerView() {
        val gridLayoutManager = GridLayoutManager(requireContext(), spanCount)
        gridLayoutManager.orientation = GridLayoutManager.VERTICAL
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == 0) {
                    spanCount
                } else if (position == favoriteCharactersAdapter.itemCount) {
                    spanCount
                } else if (position == charactersPagingAdapter.itemCount + favoriteCharactersAdapter.itemCount && footerStateAdapter.itemCount > 0) {
                    spanCount
                } else {
                    1
                }
            }
        }

        binding.characterRecyclerView.layoutManager = gridLayoutManager
        binding.characterRecyclerView.attachFab(binding.fabButton)

        charactersPagingAdapter.addLoadStateListener { loadState ->
            loadState.decideOnState(
                itemCount = charactersPagingAdapter.itemCount,
                onSuccess = { isSuccessful ->
                    if (isSuccessful) {
                        binding.characterRecyclerView.adapter = ConcatAdapter(favoriteCharactersAdapter, charactersPagingAdapter.withLoadStateFooter(
                            footer = footerStateAdapter
                        ))
                    }
                },
                onError = { isError ->
                    if (isError) {
                        binding.characterRecyclerView.adapter = ConcatAdapter(favoriteCharactersAdapter, tryAgainAdapter)
                        binding.loadingProgressBar.isVisible = false
                        binding.characterRecyclerView.isVisible = true
                    }
                }
            )
        }

        charactersPagingAdapter.setCharactersAdapterListener(object : CharactersPagingAdapter.CharactersInterface {
            override fun onClickCharacter(character: CharacterModel) {
                val action = CharactersFragmentDirections.actionCharactersFragmentToCharacterDetailsFragment(character)
                findNavController().navigate(action)
            }

            override fun onFavoriteCharacter(character: CharacterModel) {
                charactersViewModel.insertFavoriteCharacter(characterModel = character)
                Toast.makeText(requireContext(), getString(R.string.favorite_successful), Toast.LENGTH_SHORT).show()
            }

            override fun onUnfavoriteCharacter(characterId: Int) {
                charactersViewModel.deleteFavoriteCharacter(characterId = characterId)
                Toast.makeText(requireContext(), getString(R.string.un_favorite_successful), Toast.LENGTH_SHORT).show()
            }
        })

        favoriteCharactersAdapter.setFavoriteCharactersAdapterListener(object : CharactersPagingAdapter.CharactersInterface {
            override fun onClickCharacter(character: CharacterModel) {
                val action = CharactersFragmentDirections.actionCharactersFragmentToCharacterDetailsFragment(character)
                findNavController().navigate(action)
            }

            override fun onUnfavoriteCharacter(characterId: Int) {
                charactersViewModel.deleteFavoriteCharacter(characterId = characterId)
                Toast.makeText(requireContext(), getString(R.string.un_favorite_successful), Toast.LENGTH_SHORT).show()
            }
        })

        tryAgainAdapter.setTryAgainListener(object : TryAgainAdapter.TryAgainInterface {
            override fun onClickTryAgain() {
                charactersPagingAdapter.retry()
            }
        })

        footerStateAdapter.setFooterStateListener(object : TryAgainAdapter.TryAgainInterface {
            override fun onClickTryAgain() {
                charactersPagingAdapter.retry()
            }
        })
    }
}