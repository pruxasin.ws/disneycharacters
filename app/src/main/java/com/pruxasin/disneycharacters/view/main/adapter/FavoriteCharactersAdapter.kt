package com.pruxasin.disneycharacters.view.main.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.pruxasin.disneycharacters.R
import com.pruxasin.disneycharacters.databinding.ItemCharacterBinding
import com.pruxasin.disneycharacters.databinding.ItemHeaderSectionBinding
import com.pruxasin.domain.model.characters.CharacterModel
import java.util.*

class FavoriteCharactersAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val HEADER_VIEW_TYPE = 1
    val CHARACTER_VIEW_TYPE = 2

    private lateinit var charactersAdapterListener: CharactersPagingAdapter.CharactersInterface
    private var characters: List<CharacterModel> = listOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER_VIEW_TYPE -> {
                HeaderViewHolder(ItemHeaderSectionBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            } else -> {
                FavoriteCharactersViewHolder(ItemCharacterBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is FavoriteCharactersAdapter.HeaderViewHolder -> characters[position].let { holder.bindView(it.header ?: "", position) }
            is FavoriteCharactersAdapter.FavoriteCharactersViewHolder -> characters[position].let { holder.bindView(it, position) }
        }
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (!characters[position].header.isNullOrBlank()) {
            HEADER_VIEW_TYPE
        } else {
            CHARACTER_VIEW_TYPE
        }
    }

    inner class FavoriteCharactersViewHolder(private val binding: ItemCharacterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindView(character: CharacterModel, position: Int) {
            val context = binding.root.context

            binding.root.setOnClickListener {
                if (this@FavoriteCharactersAdapter::charactersAdapterListener.isInitialized) {
                    charactersAdapterListener.onClickCharacter(character = character)
                }
            }
            binding.characterPhotoImageView.load(character.imageUrl) {
                crossfade(true)
            }

            binding.characterNameTextview.text = character.name

            binding.root.setOnCreateContextMenuListener { contextMenu, _, _ ->
                contextMenu.add(context.getString(R.string.un_favorite)).setOnMenuItemClickListener {
                    if (this@FavoriteCharactersAdapter::charactersAdapterListener.isInitialized) {
                        charactersAdapterListener.onUnfavoriteCharacter(characterId = character._id)
                    }
                    true
                }
            }
        }
    }

    inner class HeaderViewHolder(private val binding: ItemHeaderSectionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindView(header: String, position: Int) {
            binding.headerSectionTextView.text = header
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(characters: List<CharacterModel>) {
        this.characters = characters
        notifyDataSetChanged()
    }

    fun setFavoriteCharactersAdapterListener(charactersAdapterListener: CharactersPagingAdapter.CharactersInterface) {
        this.charactersAdapterListener = charactersAdapterListener
    }
}