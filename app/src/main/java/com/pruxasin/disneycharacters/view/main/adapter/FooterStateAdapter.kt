package com.pruxasin.disneycharacters.view.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pruxasin.disneycharacters.databinding.ViewLoadingStateBinding

class FooterStateAdapter :
    LoadStateAdapter<FooterStateAdapter.FooterStateItemViewHolder>() {

    private lateinit var footerStateListener: TryAgainAdapter.TryAgainInterface

    inner class FooterStateItemViewHolder(
        private val binding: ViewLoadingStateBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(loadState: LoadState) {
            val isLoading = loadState is LoadState.Loading
            val isError = loadState is LoadState.Error && !(loadState as? LoadState.Error)?.error?.message.isNullOrBlank()
            val isComplete = loadState is LoadState.NotLoading

            if (isComplete) {
                binding.loadingProgressBar.visibility = View.GONE
                binding.tryAgainView.root.visibility = View.GONE
            } else if (isLoading) {
                binding.loadingProgressBar.visibility = View.VISIBLE
                binding.tryAgainView.root.visibility = View.INVISIBLE
            } else if (isError) {
                binding.loadingProgressBar.visibility = View.INVISIBLE
                binding.tryAgainView.root.visibility = View.VISIBLE
            }

            binding. tryAgainView.tryAgainButton.setOnClickListener {
                if (this@FooterStateAdapter::footerStateListener.isInitialized) {
                    footerStateListener.onClickTryAgain()
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): FooterStateItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewLoadingStateBinding.inflate(layoutInflater, parent, false)
        return FooterStateItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FooterStateItemViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    fun setFooterStateListener(footerStateListener: TryAgainAdapter.TryAgainInterface) {
        this.footerStateListener = footerStateListener
    }
}
