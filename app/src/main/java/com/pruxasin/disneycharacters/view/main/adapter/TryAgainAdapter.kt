package com.pruxasin.disneycharacters.view.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pruxasin.disneycharacters.R
import com.pruxasin.disneycharacters.databinding.ViewTryAgainBinding

class TryAgainAdapter : RecyclerView.Adapter<TryAgainAdapter.TryAgainViewHolder>() {

    private lateinit var tryAgainListener: TryAgainInterface

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TryAgainAdapter.TryAgainViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewTryAgainBinding.inflate(layoutInflater, parent, false)
        return TryAgainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TryAgainViewHolder, position: Int) {
        holder.bindView(position)
    }

    override fun getItemCount(): Int {
        return 1
    }

    inner class TryAgainViewHolder(private val binding: ViewTryAgainBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindView(position: Int) {
            val context = binding.root.context
            binding.errorTextView.text = context.getString(R.string.could_not_reach_the_server_to_fetch_all_characters)

            binding.tryAgainButton.setOnClickListener {
                if (this@TryAgainAdapter::tryAgainListener.isInitialized) {
                    tryAgainListener.onClickTryAgain()
                }
            }
        }
    }

    fun setTryAgainListener(tryAgainListener: TryAgainInterface) {
        this.tryAgainListener = tryAgainListener
    }

    interface TryAgainInterface {
        fun onClickTryAgain()
    }
}