package com.pruxasin.disneycharacters.view.detail.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.pruxasin.disneycharacters.R
import com.pruxasin.disneycharacters.databinding.FragmentCharacterDetailsBinding
import com.pruxasin.disneycharacters.util.ViewState
import com.pruxasin.disneycharacters.util.getCharacterDescription
import com.pruxasin.disneycharacters.view_model.CharacterViewModel
import com.pruxasin.disneycharacters.view_model.CharactersViewModel
import com.pruxasin.domain.model.characters.CharacterModel
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class CharacterDetailsFragment: Fragment() {
    private lateinit var binding: FragmentCharacterDetailsBinding

    private val args: CharacterDetailsFragmentArgs by navArgs()
    private val charactersViewModel: CharactersViewModel by viewModel()
    private val characterViewModel: CharacterViewModel by viewModel()
    private var favoriteCharacterIds: List<Int> = emptyList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!this::binding.isInitialized) {
            binding = FragmentCharacterDetailsBinding.inflate(layoutInflater, container, false)
            setupView()
            observeData()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        characterViewModel.getCharacter(args.character._id)
    }

    private fun setupView() {
        binding.actionBarView.arrowBackImageView.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.actionBarView.titleTextView.text = getString(R.string.character_details)

        binding.viewError.tryAgainButton.setOnClickListener {
            characterViewModel.getCharacter(args.character._id)
        }
    }

    private fun observeData() {
        charactersViewModel.getFavoriteCharacters()
            .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { characters ->
                favoriteCharacterIds = characters.map { it._id }
                updateFavoriteImageView()
            }.launchIn(lifecycleScope)

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                characterViewModel.state.collectLatest {
                    setupDetailsView(it)
                }
            }
        }
    }

    private fun setupDetailsView(viewState: ViewState<CharacterModel>) {
        when (viewState) {
            is ViewState.Loading -> {
                binding.loadingProgressBar.isVisible = true
                binding.contentScrollView.isVisible = false
                binding.viewError.root.isVisible = false
            }
            is ViewState.Success -> {
                val character = viewState.data
                binding.characterPhotoImageView.load(character.imageUrl) {
                    crossfade(true)
                }
                binding.characterPhotoImageView.setOnClickListener {
                    viewImageInFullScreenMode(listOf(character.imageUrl ?: ""))
                }

                binding.characterNameTextview.text = character.name

                binding.favoriteImageView.setColorFilter(ContextCompat.getColor(requireContext(), R.color.red), android.graphics.PorterDuff.Mode.SRC_IN)
                binding.favoriteImageView.setOnClickListener {
                    if (isFavorite()) {
                        charactersViewModel.deleteFavoriteCharacter(character._id)
                    } else {
                        charactersViewModel.insertFavoriteCharacter(character)
                    }
                }

                binding.descriptionDetailTextView.text = character.getCharacterDescription()

                binding.loadingProgressBar.isVisible = false
                binding.viewError.root.isVisible = false
                binding.contentScrollView.isVisible = true
            }
            is ViewState.Error -> {
                binding.viewError.errorTextView.text = viewState.message

                binding.loadingProgressBar.isVisible = false
                binding.contentScrollView.isVisible = false
                binding.viewError.root.isVisible = true
            }
        }
    }

    private fun updateFavoriteImageView() {
        binding.favoriteImageView.setImageResource(
            if (isFavorite()) {
                R.drawable.ic_heart_minus
            } else {
                R.drawable.ic_heart_plus_outline
            }
        )
    }

    private fun isFavorite(): Boolean {
        return favoriteCharacterIds.contains(args.character._id)
    }

    private fun viewImageInFullScreenMode(imageList: List<String>) {
        StfalconImageViewer.Builder(context, imageList) { view, image ->
            view.load(image) {
                crossfade(true)
                crossfade(500)
            }
        }.apply {
            withTransitionFrom(binding.characterPhotoImageView)
            withBackgroundColor(ContextCompat.getColor(requireContext(), R.color.black_opacity_90))
            withHiddenStatusBar(false)
            show(true)
        }
    }
}