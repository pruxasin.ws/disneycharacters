package com.pruxasin.disneycharacters.view.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.pruxasin.disneycharacters.R
import com.pruxasin.disneycharacters.databinding.ItemCharacterBinding
import com.pruxasin.disneycharacters.databinding.ItemHeaderSectionBinding
import com.pruxasin.domain.model.characters.CharacterModel

class CharactersPagingAdapter :
    PagingDataAdapter<CharacterModel, RecyclerView.ViewHolder>(CharactersComparator)
{
    val HEADER_VIEW_TYPE = 1
    val CHARACTER_VIEW_TYPE = 2

    private lateinit var charactersAdapterListener: CharactersInterface

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER_VIEW_TYPE -> {
                HeaderViewHolder(ItemHeaderSectionBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            } else -> {
                CharactersViewHolder(ItemCharacterBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (!getItem(position)?.header.isNullOrBlank()) {
            HEADER_VIEW_TYPE
        } else {
            CHARACTER_VIEW_TYPE
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is HeaderViewHolder -> getItem(position)?.let { holder.bindView(it.header ?: "", position) }
            is CharactersViewHolder -> getItem(position)?.let { holder.bindView(it, position) }
        }
    }

    inner class CharactersViewHolder(private val binding: ItemCharacterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindView(character: CharacterModel, position: Int) {
            val context = binding.root.context

            binding.root.setOnClickListener {
                if (this@CharactersPagingAdapter::charactersAdapterListener.isInitialized) {
                    charactersAdapterListener.onClickCharacter(character = character)
                }
            }
            binding.characterPhotoImageView.load(character.imageUrl) {
                crossfade(true)
            }

            binding.characterNameTextview.text = character.name

            binding.root.setOnCreateContextMenuListener { contextMenu, _, _ ->
                contextMenu.add(context.getString(R.string.favorite)).setOnMenuItemClickListener {
                    if (this@CharactersPagingAdapter::charactersAdapterListener.isInitialized) {
                        charactersAdapterListener.onFavoriteCharacter(character = character)
                    }
                    true
                }
                contextMenu.add(context.getString(R.string.un_favorite)).setOnMenuItemClickListener {
                    if (this@CharactersPagingAdapter::charactersAdapterListener.isInitialized) {
                        charactersAdapterListener.onUnfavoriteCharacter(characterId = character._id)
                    }
                    true
                }
            }
        }
    }

    inner class HeaderViewHolder(private val binding: ItemHeaderSectionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindView(header: String, position: Int) {
            binding.headerSectionTextView.text = header
        }
    }

    object CharactersComparator : DiffUtil.ItemCallback<CharacterModel>() {
        override fun areItemsTheSame(oldItem: CharacterModel, newItem: CharacterModel) =
            oldItem._id == newItem._id

        override fun areContentsTheSame(oldItem: CharacterModel, newItem: CharacterModel) =
            oldItem == newItem
    }

    fun setCharactersAdapterListener(charactersAdapterListener: CharactersInterface) {
        this.charactersAdapterListener = charactersAdapterListener
    }

    interface CharactersInterface {
        fun onClickCharacter(character: CharacterModel)
        fun onFavoriteCharacter(character: CharacterModel){ }
        fun onUnfavoriteCharacter(characterId: Int)
    }
}